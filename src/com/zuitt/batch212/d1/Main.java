package com.zuitt.batch212.d1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

//        for
        for(int i = 0; i < 10; i++){
            System.out.println("Current Count: " + i);
        }

        int[] intArray = {100, 200, 300, 400, 500};

        for(int i = 0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }

//        Another way to loop through arrays is called the foreach or the enhanced for loop
        String[] nameArray = {"John", "Paul", "George", "Ringo"};

        for(String name: nameArray){
            System.out.println(name);
        }

        for(int sample: intArray){
            System.out.println(sample);
        }

//        Nested Loops
        String[][] classroom = new String[3][3];

        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Mina";
        classroom[0][2] = "Naeyon";

        classroom[1][0] = "Luffy";
        classroom[1][1] = "Nami";
        classroom[1][2] = "Chopper";

        classroom[2][0] = "Loid";
        classroom[2][1] = "Anya";
        classroom[2][2] = "Yor";

//        Nested for loops
        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

//        While loops
        int x = 0;
        int y = 10;

        while (x < 10){
            System.out.println("Loop number: " + x);
            x++;
        }

//        Do-while loops
        do{
            System.out.println("Countdown: " + y);
            y--;
        } while (y > 0);

//        try-catch-finally statement

        Scanner input = new Scanner(System.in);

        int num1 = 0;

        try{
            System.out.println("Please enter a number from 1 to 10");
            num1 = input.nextInt();
        }
        catch (InputMismatchException e){
            System.out.println("Input is not a number");
        }
        catch (Exception e){
            System.out.println("Invalid input");
        }
        finally {
            if(num1 != 0){
                System.out.println("The number you entered is: " + num1);
            }
        }
    }
}
