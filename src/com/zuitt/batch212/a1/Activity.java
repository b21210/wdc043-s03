package com.zuitt.batch212.a1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner in  = new Scanner(System.in);
        int ansWhile = 1, ansFor = 1;
        int count = 1;
        int num = 1;

        try{
            System.out.println("Input an integer whose factorial will be computed: ");
            num = in.nextInt();
        }
        catch (InputMismatchException e){
            System.out.println("Input is not a number");
        }
        catch (Exception e){
            System.out.println("Invalid input");
        }
        finally {
            if(num != 0){
//            while loop
                while(count <= num){
                    ansWhile *= count;
                    count++;
                }
                System.out.println("The factorial of " + num + " is " + ansWhile);

//            for loop
                for(count = 1; count <= num; count++){
                    ansFor *= count;
                }
                System.out.println("The factorial of " + num + " is " + ansFor);
            }

        }

    }
}
